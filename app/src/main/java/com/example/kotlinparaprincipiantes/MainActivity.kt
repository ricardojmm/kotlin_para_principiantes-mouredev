package com.example.kotlinparaprincipiantes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * Leccion 1
         */
        //variablesConstants()
        /**
         * Leccion 2
         */
        //tiposDeDatos()
        /**
         * Leccion 3
         */
        //sentenciasIf()
        /**
         * Leccion 4
         */
        //sentenciaWhen()
        /**
         * Leccion 5
         */
        //arrays()
        /**
         * Leccion 6
         */
        //maps()
        /**
         * Leccion 7
         */
        //loops()
        /**
         * Leccion 8
         */
        //nullSafety()
        /**
         * Leccion 9
         */
        //funciones()
        /**
         * Leccion 10
         */
        clases()
    }

    /**
     * Aqui se hablara de las variables y constantes
     */
    private fun variablesConstants(a: String?){
        // Comment 1
        /**
         * Comment 2
         */

        // Variable
        var myFirstVariable = "Hello hackerman"
        var myFirstNumber = 1

        println(myFirstVariable);

        myFirstVariable = "Bienvenidos a MoureDev"
        println(myFirstVariable);

        // Error de asignacion de tipo
        //myFirstVariable = 1;

        var mySecondVariable = "Subscribete";

        mySecondVariable = myFirstVariable;

        println(mySecondVariable)

        myFirstVariable = "¿Ya te has suscrito"
        println(myFirstVariable);

        // Constants
        val myFirstConstant = "Te ha gustado el tutorial";
        println(myFirstConstant);

        val mySecondConstant = myFirstVariable;
        println(mySecondConstant);
    }

    /**
     * Types of data
     */
    private fun tiposDeDatos(){
        // String
        val myString = "Hola hackerman"
        val myString2 = "Bienvenido a MauroDev"
        val myString3 = myString + " " + myString2;

        println(myString3)

        // Integers (Byte, Short, Int, Long)
        /**
         * Byte = -127 y 128
         * Short = -32768 y 32767
         */
        val myInt = 1;
        val myInt2 = 2;
        val myInt3 = myInt + myInt2
        println(myInt3)

        // Decimales
        /**
         * Float = 32bits
         * Double = 64bits
         */
        val myFlout = 1.5f;
        val myDouble = 1.5;
        val myDouble2 = 2.6;
        val myDouble3 = 1;

        val myDouble4 = myDouble + myDouble2 + myDouble3;
        println(myDouble4)

        // Boolean
        val myBool = true
        val myBool2 = false
        println(myBool == myBool2);
    }

    /**
     * Sentence if
     */
    private fun sentenciasIf(){
        val myNumber = 69

        /**
         * Operadores condicionales
         * >        mayor que
         * <        menor que
         * >= (> =) mayor o igual que
         * <= (< =) menor o igual que
         * == (= =) igualdad
         * != (! =) desigualdad
         */

        if (myNumber<=10)
            println("$myNumber es menor o igual que 10")
        else
            println("$myNumber es mayor que 10")

        // operadores logicos
        /**
         * && operador logico "y"
         * || operador logico "o"
         * !  operador logico "no"
         */
        if (!(myNumber <= 10 && myNumber > 5) || myNumber == 53) {
            println("$myNumber es menor o igual que 10 y mayor a 5 o es igual a 53");
        }else if(myNumber == 60){
            println("$myNumber es igual que 60");
        }else if (myNumber != 70){
            println("$myNumber no es igual a 70");
        }else{
            println("$myNumber es mayor que 10 o igual o menor que 5 y no es igual a 53");
        }
    }

    /**
     * Sentence When
     */
    private fun sentenciaWhen(){
        // When con string
        val country = "EEUU";

        when (country){
            "España" -> {
                println("El idioma es español");
            }
            "Mexico" -> {
                println("El idioma es español");
            }
            "Colombia" -> {
                println("El idioma es español");
            }
            "Argentina" ->{
                println("El idioma es español");
            }
            "EEUU" -> {
                println("El idioma es ingles");
            }
            "Francia" -> {
                println("El idioma es frances");
            }
            else -> {
                println("No concertos el idioma");
            }
        }

        // When con int
        val age = 10;
        when (age){
            0, 1, 2 -> {
                println("Eres un bebe");
            }
            in 3..10 -> {
                println("Eres un niño");
            }
            in 11..17 -> {
                println("Eres un adolescente");
            }
            in 18..69 -> {
                println("Eres un adulto");
            }
            in 70..99 -> {
                println("Eres un anciano");
            }
            else -> {
                println(":happy");
            }
        }
    }

    /**
     * Arrays
     * Aqui se habla de arrays o arreglos
     */
    private fun arrays(){
        val names = "Baris"
        val surnam = "Mouze"
        val company = "MoureDev"
        val age = "32"

        // Creación de un Array
        val myArray = arrayListOf<String>();

        // Añadir datos de uno en uno
        myArray.add(names);
        myArray.add(surnam);
        myArray.add(company);
        myArray.add(age);

        println(myArray);

        // Añadir un conjunto de datos
        myArray.addAll(listOf("Hola","Binvenidos al tutorial"));
        println(myArray);

        // Acceder a datos
        val myCompany = myArray[2];
        println(myCompany);

        // Modificacion de datos
        myArray[5] = "Subscribete y activa la campana";

        // Eliminar datos
        myArray.removeAt(4);
        println(myArray);

        // Recorrer arrays
        myArray.forEach{
            println(it);
        }

        // Otras operaciones
        println(myArray.count());

        myArray.clear();
        println(myArray.count());
    }

    /**
     * Aqui vamos a hablar de mapas, también llamados diccionarios
     */
    private fun maps(){
        // Sintaxis
        var myMap: Map<String,Int> = mapOf();
        println(myMap);

        // Añadir elementos (Elimina los valores antiguos)
        //myMap = mapOf("Brais" to 1, "Pedro" to 2, "Sarha" to 5);
        myMap = mutableMapOf("Brais" to 1, "Pedro" to 2, "Sara" to 5);
        println(myMap);

        // Añadir un solo valor
        myMap["Ana"] = 7;
        myMap.put("Maria", 8);

        println(myMap);

        // Actualizar un datos
        myMap.put("Brais", 3);
        myMap["Brais"] = 4;
        println(myMap);

        myMap.put("Marcos", 3);
        println(myMap);

        // Acceso a datos
        println(myMap["Brais"]);

        // Eliminar un dato
        myMap.remove("Brais");
        println(myMap);
    }

    /**
     * Aqui vamos a hablar de loops, tambien llamados bucles
     */
    private fun loops(){
        // Bucles
        val myArray: List<String> = listOf("Hola", "Bienvenido al tutorial", "Suscribete");
        val myMap: Map<String, Int> = mutableMapOf("Brais" to 1, "Pedro" to 2, "Maria" to 5);

        // For
        for (myString in myArray){
            println(myString);
        }

        for (myElement in myMap){
            println("${myElement.key} ${myElement.value}");
        }

        for (x in 0..10){
            println(x);
        }

        for (x in 0 until 10 step 2){
            println(x);
        }

        for (x in 10 downTo 0){
            println(x);
        }

        val myNumericArray = (0..20);
        for (x in myNumericArray){
            println(x);
        }

        // While
        var x = 0;

        while (x<10){
            println(x);
            x+=2;
        }
    }

    /**
     * Aqui vamos a hablar de seguridad contra nulos (Null Safety
     */
    private fun nullSafety(){
        var myString = "MoureDev";
        //myString = null;

        println(myString);

        // Variable null safety
        var mySafetyString: String? = "MoureDev null safety";
        mySafetyString = null;

        println(mySafetyString);

        mySafetyString = "Suscribete!";
        println(mySafetyString);

        //println(if (mySafetyString!=null) mySafetyString!! else mySafetyString);

        println(mySafetyString?.length);

        mySafetyString?.let {
            println(it)
        }?:run{
            print(null)
        }
    }

    /**
     * Aqui vamos a hablar de funciones
     */
    fun funciones(){
        sayHello()
        sayHello()
        sayHello()

        sayMyName("Brais")
        sayMyName("Pedro")
        sayMyName("Sara")

        sayMyNameAndAge("Brais", 32);

        val sumResult = sumTwoNumbers(10, 5)
        println(sumResult);

        println(sumTwoNumbers(15, 9));

        println(sumTwoNumbers(10, sumTwoNumbers(5,5)))
    }

    /**
     * Funcion simple
     */
    fun sayHello(){
        println("Hola");
    }

    /**
     * Funcion con parametro o entrada
     */
    fun sayMyName(name: String){
        println("Hola, mi nombre es $name");
    }

    fun sayMyNameAndAge(name: String, age: Int){
        println("Hola, mi nombre es $name y mi edad es $age");
    }

    /**
     * funciones con un valor de retorno
     */
    fun sumTwoNumbers(firstNumber: Int, secondNumber: Int): Int{
        val sum = firstNumber + secondNumber;

        return sum;
    }

    /**
     * Aqui se va a hablar de clases
     */
    fun clases  (){
        val brais = Programmer("Brais", 32, arrayOf(Programmer.Language.KOTLIN, Programmer.Language.SWIFT))

        println(brais.name);

        val sara = Programmer("Sara", 35, arrayOf(Programmer.Language.JAVA), arrayOf(brais))

        brais.code();
        sara.code();

        println("${sara.friends?.first()?.name} es amigo de ${sara.name}");
    }
}